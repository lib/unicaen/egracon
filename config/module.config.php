<?php

use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use Laminas\Router\Http\Literal;
use UnicaenEgracon\Controller\EgraconController;
use UnicaenEgracon\Controller\EgraconControllerFactory;
use UnicaenEgracon\Controller\NoteController;
use UnicaenEgracon\Provider\Privilege\UnicaenegraconPrivileges;
use UnicaenEgracon\Service\Conversion\ConversionService;
use UnicaenEgracon\Service\Conversion\ConversionServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

const POSITION = 1000000;

return array(

    'doctrine' => [
        'driver' => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'UnicaenEgracon\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'apc',
                'paths' => [
                    __DIR__ . '/../src/UnicaenEgracon/Entity/Db/Mapping',
                ],
            ],
        ],
        'cache' => [
            'apc' => [
                'namespace' => 'EGRACON__' . __NAMESPACE__,
            ],
        ],
    ],

    'public_files' => [
        'inline_scripts' => [
        ],
        'stylesheets' => [
            '100_note' => 'css/unicaen-egracon.css',
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
);
