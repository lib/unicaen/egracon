<?php

namespace Application;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenEgracon\Controller\EgraconController;
use UnicaenEgracon\Controller\EgraconControllerFactory;
use UnicaenEgracon\Provider\Privilege\UnicaenegraconPrivileges;
use UnicaenEgracon\Service\Conversion\ConversionService;
use UnicaenEgracon\Service\Conversion\ConversionServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize'    => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => EgraconController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        UnicaenegraconPrivileges::UNICAENEGRACON_INDEX
                    ],
                ],
                [
                    'controller' => EgraconController::class,
                    'action' => [
                        'table',
                    ],
                    'privileges' => [
                        UnicaenegraconPrivileges::UNICAENEGRACON_CONVERSIONS
                    ],
                ],
                [
                    'controller' => EgraconController::class,
                    'action' => [
                        'bac-a-sable',
                    ],
                    'privileges' => [
                        UnicaenegraconPrivileges::UNICAENEGRACON_BACASABLE
                    ],
                ],
            ],
        ],
    ],
    'router' => [
        'routes' => [
            'egracon' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/egracon',
                    'defaults' => [
                        'controller' => EgraconController::class,
                        'action'     => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'table' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/table',
                            'defaults' => [
                                'controller' => EgraconController::class,
                                'action'     => 'table',
                            ],
                        ],
                    ],
                    'bac-a-sable' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/bac-a-sable',
                            'defaults' => [
                                'controller' => EgraconController::class,
                                'action'     => 'bac-a-sable',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'navigation'      => [
        'default' => [
            'home' => [
                'pages' => [
                    'administration' => [
                        'pages' => [
                            'egracon' => [
                                'label'    => "Gestion de notes (egracon)",
                                'route'    => 'egracon',
                                'resource' => PrivilegeController::getResourceId(EgraconController::class, 'index') ,
                                'order'    => POSITION,
                                'dropdown-header' => true,
                            ],
                            'egracon-conversions' => [
                                'label'    => "Table de conversion",
                                'route'    => 'egracon/table',
                                'resource' => PrivilegeController::getResourceId(EgraconController::class, 'table') ,
                                'order'    => POSITION + 250,
                                'icon' => 'fas fa-angle-right',
                            ],
                            'egracon-playground' => [
                                'label'    => "Bac à sable",
                                'route'    => 'egracon/bac-a-sable',
                                'resource' => PrivilegeController::getResourceId(EgraconController::class, 'bac-a-sable') ,
                                'order'    => POSITION + 300,
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            ConversionService::class => ConversionServiceFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
        ],
    ],
    'hydrators' => [
        'invokables' => [
        ],
    ],
    'controllers' => [
        'factories' => [
            EgraconController::class => EgraconControllerFactory::class
        ],
    ],
    'view_helpers' => [
        'invokables' => [
        ],
    ],
];