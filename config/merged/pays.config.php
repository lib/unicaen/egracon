<?php

namespace Application;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenEgracon\Controller\PaysController;
use UnicaenEgracon\Controller\PaysControllerFactory;
use UnicaenEgracon\Form\Pays\PaysForm;
use UnicaenEgracon\Form\Pays\PaysFormFactory;
use UnicaenEgracon\Form\Pays\PaysHydrator;
use UnicaenEgracon\Form\Pays\PaysHydratorFactory;
use UnicaenEgracon\Provider\Privilege\UnicaenegraconpaysPrivileges;
use UnicaenEgracon\Service\Pays\PaysService;
use UnicaenEgracon\Service\Pays\PaysServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => PaysController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        UnicaenegraconpaysPrivileges::UNICAENEGRACONPAYS_INDEX,
                    ],
                ],
                [
                    'controller' => PaysController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        UnicaenegraconpaysPrivileges::UNICAENEGRACONPAYS_AFFICHER,
                    ],
                ],
                [
                    'controller' => PaysController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        UnicaenegraconpaysPrivileges::UNICAENEGRACONPAYS_AJOUTER,
                    ],
                ],
                [
                    'controller' => PaysController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        UnicaenegraconpaysPrivileges::UNICAENEGRACONPAYS_MODIFIER,
                    ],
                ],
                [
                    'controller' => PaysController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        UnicaenegraconpaysPrivileges::UNICAENEGRACONPAYS_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'egracon' => [
                'child_routes' => [
                    'pays' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/pays',
                            'defaults' => [
                                /** @see PaysController::ajouterAction() */
                                'controller' => PaysController::class,
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route'    => '/afficher/:pays',
                                    'defaults' => [
                                        /** @see PaysController::afficherAction() */
                                        'controller' => PaysController::class,
                                        'action'     => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route'    => '/ajouter',
                                    'defaults' => [
                                        /** @see PaysController::ajouterAction() */
                                        'controller' => PaysController::class,
                                        'action'     => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route'    => '/modifier/:pays',
                                    'defaults' => [
                                        /** @see PaysController::modifierAction() */
                                        'controller' => PaysController::class,
                                        'action'     => 'modifier',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route'    => '/supprimer/:pays',
                                    'defaults' => [
                                        /** @see PaysController::supprimerAction() */
                                        'controller' => PaysController::class,
                                        'action'     => 'supprimer',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ],
            ],
        ],
    ],

    'navigation'      => [
        'default' => [
            'home' => [
                'pages' => [
                    'administration' => [
                        'pages' => [
                            'egracon-pays' => [
                                'label'    => "Gestion de pays",
                                'route'    => 'egracon/pays',
                                'resource' => PrivilegeController::getResourceId(PaysController::class, 'index') ,
                                'order'    => POSITION + 100,
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            PaysService::class => PaysServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            PaysController::class => PaysControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            PaysForm::class => PaysFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            PaysHydrator::class => PaysHydratorFactory::class
        ],
    ],
    'view_helpers' => [
        'invokables' => [],
    ],

];