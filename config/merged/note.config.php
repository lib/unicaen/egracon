<?php

namespace Application;

use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;
use UnicaenEgracon\Controller\NoteController;
use UnicaenEgracon\Controller\NoteControllerFactory;
use UnicaenEgracon\Form\Note\NoteForm;
use UnicaenEgracon\Form\Note\NoteFormFactory;
use UnicaenEgracon\Form\Note\NoteHydrator;
use UnicaenEgracon\Form\Note\NoteHydratorFactory;
use UnicaenEgracon\Provider\Privilege\UnicaenegraconnotePrivileges;
use UnicaenEgracon\Service\Note\NoteService;
use UnicaenEgracon\Service\Note\NoteServiceFactory;
use UnicaenEgracon\View\Helper\NoteViewHelper;
use UnicaenPrivilege\Guard\PrivilegeController;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => NoteController::class,
                    'action' => [
                        'index',
                    ],
                    'privileges' => [
                        UnicaenegraconnotePrivileges::UNICAENEGRACONNOTE_INDEX,
                    ],
                ],
                [
                    'controller' => NoteController::class,
                    'action' => [
                        'afficher',
                    ],
                    'privileges' => [
                        UnicaenegraconnotePrivileges::UNICAENEGRACONNOTE_AFFICHER,
                    ],
                ],
                [
                    'controller' => NoteController::class,
                    'action' => [
                        'ajouter',
                    ],
                    'privileges' => [
                        UnicaenegraconnotePrivileges::UNICAENEGRACONNOTE_AJOUTER,
                    ],
                ],
                [
                    'controller' => NoteController::class,
                    'action' => [
                        'modifier',
                    ],
                    'privileges' => [
                        UnicaenegraconnotePrivileges::UNICAENEGRACONNOTE_MODIFIER,
                    ],
                ],
                [
                    'controller' => NoteController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        UnicaenegraconnotePrivileges::UNICAENEGRACONNOTE_SUPPRIMER,
                    ],
                ],
            ],
        ],
    ],

    'router'          => [
        'routes' => [
            'egracon' => [
                'child_routes' => [
                    'note' => [
                        'type' => Literal::class,
                        'options' => [
                            'route'    => '/note',
                            'defaults' => [
                                /** @see NoteController::ajouterAction() */
                                'controller' => NoteController::class,
                                'action'     => 'index',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'afficher' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route'    => '/afficher/:note',
                                    'defaults' => [
                                        /** @see NoteController::afficherAction() */
                                        'controller' => NoteController::class,
                                        'action'     => 'afficher',
                                    ],
                                ],
                            ],
                            'ajouter' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route'    => '/ajouter',
                                    'defaults' => [
                                        /** @see NoteController::ajouterAction() */
                                        'controller' => NoteController::class,
                                        'action'     => 'ajouter',
                                    ],
                                ],
                            ],
                            'modifier' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route'    => '/modifier/:note',
                                    'defaults' => [
                                        /** @see NoteController::modifierAction() */
                                        'controller' => NoteController::class,
                                        'action'     => 'modifier',
                                    ],
                                ],
                            ],
                            'supprimer' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route'    => '/supprimer/:note',
                                    'defaults' => [
                                        /** @see NoteController::supprimerAction() */
                                        'controller' => NoteController::class,
                                        'action'     => 'supprimer',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ],
            ],
        ],
    ],

    'navigation'      => [
        'default' => [
            'home' => [
                'pages' => [
                    'administration' => [
                        'pages' => [
                            'egracon-notes' => [
                                'label'    => "Gestion des notes",
                                'route'    => 'egracon/note',
                                'resource' => PrivilegeController::getResourceId(NoteController::class, 'index') ,
                                'order'    => POSITION + 200,
                                'icon' => 'fas fa-angle-right',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            NoteService::class => NoteServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            NoteController::class => NoteControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            NoteForm::class => NoteFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            NoteHydrator::class => NoteHydratorFactory::class,
        ],
    ],
    'view_helpers' => [
        'invokables' => [
            NoteViewHelper::class => NoteViewHelper::class,
        ],
        'aliases' => [
            'note' => NoteViewHelper::class,
        ],
    ],

];