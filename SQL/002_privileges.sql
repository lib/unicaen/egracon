INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('unicaenegracon','Egracon - Gestion du module',10000,'UnicaenEgracon\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'unicaenegracon_index', 'Accéder à la fiche descriptive', 10 UNION
    SELECT 'unicaenegracon_conversions', 'Accéder à la table de conversion', 20 UNION
    SELECT 'unicaenegracon_bacasable', 'Accéder au bac à sable', 30
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'unicaenegracon';

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('unicaenegraconpays', 'Egracon - Gestion des pays', 10100, 'UnicaenEgracon\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'unicaenegraconpays_index', 'Accéder à l''index', 10 UNION
    SELECT 'unicaenegraconpays_afficher', 'Afficher', 20 UNION
    SELECT 'unicaenegraconpays_ajouter', 'Ajouter', 30 UNION
    SELECT 'unicaenegraconpays_modifier', 'Modifier', 40 UNION
    SELECT 'unicaenegraconpays_supprimer', 'Supprimer', 50
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'unicaenegraconpays';

INSERT INTO unicaen_privilege_categorie (code, libelle, ordre, namespace)
VALUES ('unicaenegraconnote', 'Egracon - Gestion des notes', 10200, 'UnicaenEgracon\Provider\Privilege');
INSERT INTO unicaen_privilege_privilege(CATEGORIE_ID, CODE, LIBELLE, ORDRE)
WITH d(code, lib, ordre) AS (
    SELECT 'unicaenegraconnote_index', 'Accéder à l''index', 10 UNION
    SELECT 'unicaenegraconnote_afficher', 'Afficher', 20 UNION
    SELECT 'unicaenegraconnote_ajouter', 'Ajouter', 30 UNION
    SELECT 'unicaenegraconnote_modifier', 'Modifier', 40 UNION
    SELECT 'unicaenegraconnote_supprimer', 'Supprimer', 50
)
SELECT cp.id, d.code, d.lib, d.ordre
FROM d
JOIN unicaen_privilege_categorie cp ON cp.CODE = 'unicaenegraconnote';