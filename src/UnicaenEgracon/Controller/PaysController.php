<?php

namespace UnicaenEgracon\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenEgracon\Entity\Db\Pays;
use UnicaenEgracon\Form\Pays\PaysFormAwareTrait;
use UnicaenEgracon\Service\Note\NoteServiceAwareTrait;
use UnicaenEgracon\Service\Pays\PaysServiceAwareTrait;

class PaysController extends AbstractActionController {
    use NoteServiceAwareTrait;
    use PaysServiceAwareTrait;
    use PaysFormAwareTrait;

    public function indexAction() : ViewModel
    {
        $paysArray = $this->getPaysService()->getAllPays();
        return new ViewModel([
            'paysArray' => $paysArray,
        ]);
    }

    public function afficherAction() : ViewModel
    {
        $pays = $this->getPaysService()->getRequestedPays($this);

        $keyReference = "France";
        $keyPays = $pays->getLibelle();

        $notes = $this->getNoteService()->getNotesReferences();
        $conversions[$keyReference] = [];
        $conversions[$keyPays] = [];
        foreach ($notes as $note) {
            $conversions[$keyReference][] = $note;
            $conversions[$keyPays][] = $note->getConversionByPays($pays);
        }


        return new ViewModel([
            'title' => "Affichage d'un pays",
            'pays' => $pays,
            'conversions' => $conversions,
        ]);
    }

    public function ajouterAction() : ViewModel
    {
        $pays = new Pays();

        $form = $this->getPaysForm();
        $form->setAttribute('action', $this->url()->fromRoute('egracon/pays/ajouter', [], [], true));
        $form->bind($pays);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getPaysService()->create($pays);
                exit();
            }
        }
        $vm = new ViewModel([
            'title' => "Ajouter un pays",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-egracon/default/default-form');
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $pays = $this->getPaysService()->getRequestedPays($this);

        $form = $this->getPaysForm();
        $form->setAttribute('action', $this->url()->fromRoute('egracon/pays/modifier', ['pays' => $pays->getId()], [], true));
        $form->bind($pays);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getPaysService()->update($pays);
                exit();
            }
        }
        $vm = new ViewModel([
            'title' => "Modifier le pays [".$pays->getLibelle()."]",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-egracon/default/default-form');
        return $vm;
    }

    public function supprimerAction(): ViewModel
    {
        $pays = $this->getPaysService()->getRequestedPays($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getPaysService()->delete($pays);
            exit();
        }

        $vm = new ViewModel();
        if ($pays !== null) {
            $vm->setTemplate('unicaen-egracon/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du pays " . $pays->getLibelle(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('egracon/pays/supprimer', ["pays" => $pays->getId()], [], true),
            ]);
        }
        return $vm;
    }
}