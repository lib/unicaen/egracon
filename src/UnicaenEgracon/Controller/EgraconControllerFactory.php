<?php

namespace UnicaenEgracon\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEgracon\Service\Conversion\ConversionService;
use UnicaenEgracon\Service\Note\NoteService;
use UnicaenEgracon\Service\Pays\PaysService;


class EgraconControllerFactory
{
    /**
     * @param ContainerInterface $container
     * @return EgraconController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : EgraconController
    {
        /**
         * @var ConversionService $conversionService
         * @var NoteService $noteService
         * @var PaysService $paysService
         */
        $conversionService = $container->get(ConversionService::class);
        $noteService = $container->get(NoteService::class);
        $paysService = $container->get(PaysService::class);

        $controller = new EgraconController();
        $controller->setConversionService($conversionService);
        $controller->setNoteService($noteService);
        $controller->setPaysService($paysService);
        return $controller;
    }
}