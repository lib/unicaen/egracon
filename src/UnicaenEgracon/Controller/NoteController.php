<?php

namespace UnicaenEgracon\Controller;

use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenEgracon\Entity\Db\Note;
use UnicaenEgracon\Form\Note\NoteFormAwareTrait;
use UnicaenEgracon\Service\Note\NoteServiceAwareTrait;

class NoteController extends AbstractActionController {
    use NoteServiceAwareTrait;
    use NoteFormAwareTrait;

    public function indexAction() : ViewModel
    {
        $notes = $this->getNoteService()->getNotes();
        return new ViewModel([
            'notes' => $notes,
        ]);
    }

    public function afficherAction() : ViewModel
    {
        $note = $this->getNoteService()->getRequestedNote($this);

        return new ViewModel([
            'title' => "Affichage d'une note",
            'note' => $note,
        ]);
    }

    public function ajouterAction() : ViewModel
    {
        $note = new Note();

        $form = $this->getNoteForm();
        $form->setAttribute('action', $this->url()->fromRoute('egracon/note/ajouter', [], [], true));
        $form->bind($note);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getNoteService()->create($note);
                exit();
            }
        }
        $vm = new ViewModel([
            'title' => "Ajouter un note",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-egracon/default/default-form');
        return $vm;
    }

    public function modifierAction() : ViewModel
    {
        $note = $this->getNoteService()->getRequestedNote($this);

        $form = $this->getNoteForm();
        $form->setAttribute('action', $this->url()->fromRoute('egracon/note/modifier', ['note' => $note->getId()], [], true));
        $form->bind($note);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $form->setData($data);
            if ($form->isValid()) {
                $this->getNoteService()->update($note);
                exit();
            }
        }
        $vm = new ViewModel([
            'title' => "Modifier le note [".$note->getId()."]",
            'form' => $form,
        ]);
        $vm->setTemplate('unicaen-egracon/default/default-form');
        return $vm;
    }

    public function supprimerAction(): ViewModel
    {
        $note = $this->getNoteService()->getRequestedNote($this);

        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            if ($data["reponse"] === "oui") $this->getNoteService()->delete($note);
            exit();
        }

        $vm = new ViewModel();
        if ($note !== null) {
            $vm->setTemplate('unicaen-egracon/default/confirmation');
            $vm->setVariables([
                'title' => "Suppression du note " . $note->getId(),
                'text' => "La suppression est définitive êtes-vous sûr&middot;e de vouloir continuer ?",
                'action' => $this->url()->fromRoute('egracon/note/supprimer', ["note" => $note->getId()], [], true),
            ]);
        }
        return $vm;
    }
}