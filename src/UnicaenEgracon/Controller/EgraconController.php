<?php

namespace UnicaenEgracon\Controller;

use Exception;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;
use UnicaenEgracon\Service\Conversion\ConversionServiceAwareTrait;
use UnicaenEgracon\Service\Note\NoteServiceAwareTrait;
use UnicaenEgracon\Service\Pays\PaysServiceAwareTrait;

class EgraconController extends AbstractActionController {

    use ConversionServiceAwareTrait;
    use NoteServiceAwareTrait;
    use PaysServiceAwareTrait;

    public function indexAction() : ViewModel
    {
        return new ViewModel([]);
    }

    public function bacASableAction() : ViewModel
    {
        $allPays = $this->getPaysService()->getAllPays();
        $paysCode = $this->params()->fromQuery('pays');
        if ($paysCode === '') $paysCode = null; else $paysCode=strtoupper($paysCode);
        $valeur = $this->params()->fromQuery('valeur');
        if ($valeur === '') {
            $valeur = null;
        }else if($valeur !== null){
            $valeur = str_replace(",",".", $valeur);
        }

        $message = null;
        $conversion = null;
        try {
            $conversion = $this->getConversionService()->convert($valeur, $paysCode);

        } catch (Exception $e) {
            $message = $e->getMessage();
        }
        return new ViewModel([
            'allPays' => $allPays,
            'paysCode' => $paysCode,
            'valeur' => $valeur,
            'conversion' => $conversion,
            'message' => $message,
        ]);
    }

    public function tableAction() : ViewModel
    {
        $allPays = $this->getPaysService()->getAllPays();

        $keyReference = "France";
        $conversions[$keyReference] = [];
        foreach ($allPays as $pays) {
            if ($pays->getLibelle() !== $keyReference) $conversions[$pays->getLibelle()] = [];
        }

        $notes = $this->getNoteService()->getNotesReferences();

        foreach ($notes as $note) {
            $conversions[$keyReference][] = $note;
            foreach ($allPays as $pays) {
                if ($pays->getLibelle() !== $keyReference) {
                    $conversion = $note->getConversionByPays($pays);
                    $conversions[$pays->getLibelle()][] = $conversion;
                }
            }

        }

        return new ViewModel([
            'pays' => $allPays,
            'conversions' => $conversions,
        ]);

    }
}