<?php

namespace UnicaenEgracon\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEgracon\Form\Note\NoteForm;
use UnicaenEgracon\Service\Note\NoteService;

class NoteControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return NoteController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : NoteController
    {
        /**
         * @var NoteService $noteService
         * @var NoteForm $noteForm
         */
        $noteService = $container->get(NoteService::class);
        $noteForm = $container->get('FormElementManager')->get(NoteForm::class);

        $controller = new NoteController();
        $controller->setNoteService($noteService);
        $controller->setNoteForm($noteForm);
        return $controller;
    }
}