<?php

namespace UnicaenEgracon\Controller;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEgracon\Form\Pays\PaysForm;
use UnicaenEgracon\Service\Note\NoteService;
use UnicaenEgracon\Service\Pays\PaysService;

class PaysControllerFactory {

    /**
     * @param ContainerInterface $container
     * @return PaysController
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : PaysController
    {
        /**
         * @var NoteService $noteService
         * @var PaysService $paysService
         * @var PaysForm $paysForm
         */
        $noteService = $container->get(NoteService::class);
        $paysService = $container->get(PaysService::class);
        $paysForm = $container->get('FormElementManager')->get(PaysForm::class);

        $controller = new PaysController();
        $controller->setNoteService($noteService);
        $controller->setPaysService($paysService);
        $controller->setPaysForm($paysForm);
        return $controller;
    }
}