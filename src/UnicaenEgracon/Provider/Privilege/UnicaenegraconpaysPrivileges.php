<?php

namespace UnicaenEgracon\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class UnicaenegraconpaysPrivileges extends Privileges
{
    const UNICAENEGRACONPAYS_INDEX = 'unicaenegraconpays-unicaenegraconpays_index';
    const UNICAENEGRACONPAYS_AFFICHER = 'unicaenegraconpays-unicaenegraconpays_afficher';
    const UNICAENEGRACONPAYS_AJOUTER = 'unicaenegraconpays-unicaenegraconpays_ajouter';
    const UNICAENEGRACONPAYS_MODIFIER = 'unicaenegraconpays-unicaenegraconpays_modifier';
    const UNICAENEGRACONPAYS_SUPPRIMER = 'unicaenegraconpays-unicaenegraconpays_supprimer';
}