<?php

namespace UnicaenEgracon\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class UnicaenegraconPrivileges extends Privileges
{
    const UNICAENEGRACON_INDEX          = 'unicaenegracon-unicaenegracon_index';
    const UNICAENEGRACON_CONVERSIONS    = 'unicaenegracon-unicaenegracon_conversions';
    const UNICAENEGRACON_BACASABLE      = 'unicaenegracon-unicaenegracon_bacasable';
}