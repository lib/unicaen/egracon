<?php

namespace UnicaenEgracon\Provider\Privilege;

use UnicaenPrivilege\Provider\Privilege\Privileges;

class UnicaenegraconnotePrivileges extends Privileges
{
    const UNICAENEGRACONNOTE_INDEX = 'unicaenegraconnote-unicaenegraconnote_index';
    const UNICAENEGRACONNOTE_AFFICHER = 'unicaenegraconnote-unicaenegraconnote_afficher';
    const UNICAENEGRACONNOTE_AJOUTER = 'unicaenegraconnote-unicaenegraconnote_ajouter';
    const UNICAENEGRACONNOTE_MODIFIER = 'unicaenegraconnote-unicaenegraconnote_modifier';
    const UNICAENEGRACONNOTE_SUPPRIMER = 'unicaenegraconnote-unicaenegraconnote_supprimer';
}