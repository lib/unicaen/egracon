<?php

namespace UnicaenEgracon\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

class Pays {

    private ?int $id = null;
    private ?string $libelle = null;
    private ?string $code = null;

    private Collection $notes;

    public function __construct()
    {
        $this->notes = new ArrayCollection();
    }
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): void
    {
        $this->libelle = $libelle;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /** @return Note[] */
    public function getNotes() : array
    {
        return $this->notes->toArray();
    }

}