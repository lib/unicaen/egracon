<?php

namespace UnicaenEgracon\Entity\Db;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenEgracon\Service\Note\NoteService;

class Note {
    private ?int $id = null;
    private ?string $valeurBasse = null;
    private ?string $valeurHaute = null;
    private ?string $description = null;

    private ?Pays $pays = null;
    private ?Note $reference = null;
    private Collection $conversions;

    public function __construct()
    {
        $this->conversions = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValeurBasse(): ?string
    {
        return $this->valeurBasse;
    }

    public function setValeurBasse(?string $valeurBasse): void
    {
        $this->valeurBasse = $valeurBasse;
    }

    public function getValeurHaute(): ?string
    {
        return $this->valeurHaute;
    }

    public function setValeurHaute(?string $valeurHaute): void
    {
        $this->valeurHaute = $valeurHaute;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getPays(): ?Pays
    {
        return $this->pays;
    }

    public function setPays(?Pays $pays): void
    {
        $this->pays = $pays;
    }

    public function getReference(): ?Note
    {
        return $this->reference;
    }

    public function setReference(?Note $reference): void
    {
        $this->reference = $reference;
    }

    /** @return  Note[] */
    public function getConversions() : array
    {
        return $this->conversions->toArray();
    }

    public function getConversionByPays(?Pays $pays) : ?Note
    {
        /** @var Note $conversion */
        foreach ($this->conversions as $conversion) {
            if ($conversion->getPays() === $pays) return $conversion;
        }
        return null;
    }


}