<?php

namespace UnicaenEgracon\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use UnicaenEgracon\Entity\Db\Note;

class NoteViewHelper extends AbstractHelper {

    /**
     * Options :
     * - 'AsText' (default : false) > Affichage de la note sous la forme d'une chaîne de caractère
     */
    public function __invoke(?Note $note, array $options = []) : string
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('note', ['note' => $note, 'options' => $options]);
    }
}