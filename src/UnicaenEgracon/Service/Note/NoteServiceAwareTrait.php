<?php

namespace UnicaenEgracon\Service\Note;

trait NoteServiceAwareTrait {

    private NoteService $noteService;

    public function getNoteService(): NoteService
    {
        return $this->noteService;
    }

    public function setNoteService(NoteService $noteService): void
    {
        $this->noteService = $noteService;
    }

}