<?php

namespace UnicaenEgracon\Service\Note;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class NoteServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return NoteService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : NoteService
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new NoteService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}