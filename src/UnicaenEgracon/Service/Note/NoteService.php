<?php

namespace UnicaenEgracon\Service\Note;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenEgracon\Entity\Db\Note;
use UnicaenEgracon\Entity\Db\Pays;

class NoteService {
    use ProvidesObjectManager;

    /** GESTION DES ENTITES *******************************/

    public function create(Note $note) : Note
    {
        try {
            $this->getObjectManager()->persist($note);
            $this->getObjectManager()->flush($note);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue en base de donnée", 0, $e);
        }
        return $note;
    }

    public function update(Note $note) : Note
    {
        try {
            $this->getObjectManager()->flush($note);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue en base de donnée", 0, $e);
        }
        return $note;
    }

    public function delete(Note $note) : Note
    {
        try {
            $this->getObjectManager()->remove($note);
            $this->getObjectManager()->flush($note);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue en base de donnée", 0, $e);
        }
        return $note;
    }

    /** QUERYING ******************************************/

    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Note::class)->createQueryBuilder('note')
            ->join('note.pays', 'pays')
            ->leftJoin('note.reference', 'reference');
        return $qb;
    }

    public function getNote(?int $id) : ?Note
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('note.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Note partagent le même id [".$id."]",0,$e);
        }
        return $result;
    }

    public function getRequestedNote(AbstractActionController $controller, ?string $param = "note") : ?Note
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getNote($id);
    }

    /** @return Note[] */
    public function getNotes(string $champ = 'id', string $ordre = 'ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('note.'.$champ, $ordre);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    /** @return Note[] */
    public function getNotesByPaysCode(string $paysCode, string $champ = 'id', string $ordre = 'ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('pays.code = :pays')->setParameter('pays', $paysCode)
            ->orderBy('note.'.$champ, $ordre);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getNotesAsOption() : array
    {
        $notes = $this->getNotes();

        $option = [];
        foreach ($notes as $note) {
            $option[$note->getId()] = $note->getPays()->getLibelle(). " | " . $note->getValeurBasse() ." - ". $note->getValeurHaute();
        }
        return $option;
    }

    /** @return Note[] */
    public function getNotesReferences() : array
    {
        $notes = $this->getNotesByPaysCode('FR', 'valeurBasse', 'ASC');
        return $notes;
    }

    public function getNotesReferencesAsOption() : array
    {
        $notes = $this->getNotesReferences();
        $option = [];
        foreach ($notes as $note) {
            $option[$note->getId()] = $note->getPays()->getLibelle(). " | " . $note->getValeurBasse() ." - ". $note->getValeurHaute();
        }
        return $option;
    }
}