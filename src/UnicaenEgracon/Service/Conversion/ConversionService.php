<?php

namespace UnicaenEgracon\Service\Conversion;

use RuntimeException;
use UnicaenEgracon\Entity\Db\Note;
use UnicaenEgracon\Service\Note\NoteServiceAwareTrait;
use UnicaenEgracon\Service\Pays\PaysServiceAwareTrait;

class ConversionService {
    use NoteServiceAwareTrait;
    use PaysServiceAwareTrait;

    public function convert(?float $valeur, ?string $paysCode) : ?Note
    {
        if ($valeur === null) throw new RuntimeException("Aucune valeur à convertir de passer à la méthode");
        if ($paysCode === null) throw new RuntimeException("Aucun code pays à convertir de passer à la méthode");

        $pays = $this->getPaysService()->getPaysByCode($paysCode);
        if ($pays === null) throw new RuntimeException("Aucun pays de remonté avec le code [".$paysCode."]");

        $notes = $this->getNoteService()->getNotesReferences();
        $note = null;
        foreach ($notes as $note_) {
            if ( ((float) $note_->getValeurBasse()) <= $valeur AND $note_->getValeurHaute() >= $valeur) {
                $note = $note_;
                break;
            }
        }
        if ($note === null) throw new RuntimeException("Aucune note de référence remontée avec la valeur [".$valeur."]");

        foreach ($note->getConversions() as $conversion) {
            if ($conversion->getPays() === $pays) return $conversion;
        }

        throw new RuntimeException("Aucune conversion déclarée pour le pays [".$pays->getLibelle()."] et la valeur [".$valeur."]");

    }
}