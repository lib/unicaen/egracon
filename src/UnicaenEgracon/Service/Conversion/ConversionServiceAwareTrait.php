<?php

namespace UnicaenEgracon\Service\Conversion;

trait ConversionServiceAwareTrait {

    private ConversionService $conversionService;

    public function getConversionService(): ConversionService
    {
        return $this->conversionService;
    }

    public function setConversionService(ConversionService $conversionService): void
    {
        $this->conversionService = $conversionService;
    }

}