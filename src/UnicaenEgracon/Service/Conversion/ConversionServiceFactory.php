<?php

namespace UnicaenEgracon\Service\Conversion;

use Psr\Container\ContainerInterface;
use UnicaenEgracon\Service\Note\NoteService;
use UnicaenEgracon\Service\Pays\PaysService;

class ConversionServiceFactory {

    public function __invoke(ContainerInterface $container) : ConversionService
    {
        /**
         * @var NoteService $noteService
         * @var PaysService $paysService
         */
        $noteService = $container->get(NoteService::class);
        $paysService = $container->get(PaysService::class);

        $service = new ConversionService();
        $service->setNoteService($noteService);
        $service->setPaysService($paysService);
        return $service;
    }
}