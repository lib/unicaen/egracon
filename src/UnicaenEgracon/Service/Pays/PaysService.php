<?php

namespace UnicaenEgracon\Service\Pays;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\QueryBuilder;
use DoctrineModule\Persistence\ProvidesObjectManager;
use Laminas\Mvc\Controller\AbstractActionController;
use RuntimeException;
use UnicaenEgracon\Entity\Db\Pays;

class PaysService {
    use ProvidesObjectManager;

    /** GESTION DES ENTITES *******************************/

    public function create(Pays $pays) : Pays
    {
        try {
            $this->getObjectManager()->persist($pays);
            $this->getObjectManager()->flush($pays);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue en base de donnée", 0, $e);
        }
        return $pays;
    }

    public function update(Pays $pays) : Pays
    {
        try {
            $this->getObjectManager()->flush($pays);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue en base de donnée", 0, $e);
        }
        return $pays;
    }

    public function delete(Pays $pays) : Pays
    {
        try {
            $this->getObjectManager()->remove($pays);
            $this->getObjectManager()->flush($pays);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenue en base de donnée", 0, $e);
        }
        return $pays;
    }

    /** QUERYING ******************************************/

    public function createQueryBuilder() : QueryBuilder
    {
        $qb = $this->getObjectManager()->getRepository(Pays::class)->createQueryBuilder('pays')
            ->leftJoin('pays.notes', 'note');
        return $qb;
    }

    public function getPays(?int $id) : ?Pays
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('pays.id = :id')->setParameter('id', $id);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Pays partagent le même id [".$id."]",0,$e);
        }
        return $result;
    }

    public function getPaysByCode(string $paysCode) : ?Pays
    {
        $qb = $this->createQueryBuilder()
            ->andWhere('pays.code = :code')->setParameter('code', $paysCode);
        try {
            $result = $qb->getQuery()->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            throw new RuntimeException("Plusieurs Pays partagent le même code [".$paysCode."]",0,$e);
        }
        return $result;
    }

    public function getRequestedPays(AbstractActionController $controller, ?string $param = "pays") : ?Pays
    {
        $id = $controller->params()->fromRoute($param);
        return $this->getPays($id);
    }

    /** @return Pays[] */
    public function getAllPays(string $champ = 'libelle', string $ordre = 'ASC') : array
    {
        $qb = $this->createQueryBuilder()
            ->orderBy('pays.'.$champ, $ordre);
        $result = $qb->getQuery()->getResult();
        return $result;
    }

    public function getPaysAsOption() : array
    {
        $pays = $this->getAllPays();

        $option = [];
        foreach ($pays as $pay) {
            $option[$pay->getId()] = $pay->getLibelle();
        }
        return $option;
    }


}