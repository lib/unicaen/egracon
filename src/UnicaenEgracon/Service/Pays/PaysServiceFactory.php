<?php

namespace UnicaenEgracon\Service\Pays;

use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class PaysServiceFactory {

    /**
     * @param ContainerInterface $container
     * @return PaysService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : PaysService
    {
        /** @var EntityManager $entityManager */
        $entityManager = $container->get('doctrine.entitymanager.orm_default');

        $service = new PaysService();
        $service->setObjectManager($entityManager);
        return $service;
    }
}