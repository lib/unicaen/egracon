<?php

namespace UnicaenEgracon\Service\Pays;

trait PaysServiceAwareTrait {

    private PaysService $paysService;

    public function getPaysService(): PaysService
    {
        return $this->paysService;
    }

    public function setPaysService(PaysService $paysService): void
    {
        $this->paysService = $paysService;
    }

}