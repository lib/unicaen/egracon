<?php

namespace UnicaenEgracon\Form\Note;


trait NoteFormAwareTrait {

    private NoteForm $noteForm;

    public function getNoteForm(): NoteForm
    {
        return $this->noteForm;
    }

    public function setNoteForm(NoteForm $noteForm): void
    {
        $this->noteForm = $noteForm;
    }


}