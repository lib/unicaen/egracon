<?php

namespace UnicaenEgracon\Form\Note;

use Laminas\Hydrator\HydratorInterface;
use UnicaenEgracon\Entity\Db\Note;
use UnicaenEgracon\Service\Note\NoteServiceAwareTrait;
use UnicaenEgracon\Service\Pays\PaysServiceAwareTrait;

class NoteHydrator implements HydratorInterface {
    use NoteServiceAwareTrait;
    use PaysServiceAwareTrait;

    /**
     * @param Note $object
     * @return array
     */
    public function extract(object $object): array
    {
        $data = [
            'valeur_basse' => $object->getValeurBasse(),
            'valeur_haute' => $object->getValeurHaute(),
            'description' => $object->getDescription(),

            'pays' => ($object->getPays())?$object->getPays()->getId():null,
            'reference' => ($object->getReference())?$object->getReference()->getId():null,
        ];
        return $data;
    }

    /**
     * @param array $data
     * @param Note $object
     * @return Note
     */
    public function hydrate(array $data, object $object) : object
    {
        $valeurBasse = (isset($data['valeur_basse']) AND trim($data['valeur_basse']) !== '')?trim($data['valeur_basse']):null;
        $valeurHaute = (isset($data['valeur_haute']) AND trim($data['valeur_haute']) !== '')?trim($data['valeur_haute']):null;
        $description = (isset($data['description']) AND trim($data['description']) !== '')?trim($data['description']):null;

        $pays = (isset($data['pays']) AND trim($data['pays']) !== '')?$this->getPaysService()->getPays($data['pays']):null;
        $reference = (isset($data['reference']) AND trim($data['reference']) !== '')?$this->getNoteService()->getNote($data['reference']):null;

        $object->setValeurBasse($valeurBasse);
        $object->setValeurHaute($valeurHaute);
        $object->setDescription($description);
        $object->setPays($pays);
        $object->setReference($reference);
        return $object;
    }


}