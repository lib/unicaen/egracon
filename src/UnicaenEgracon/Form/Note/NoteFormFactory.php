<?php

namespace UnicaenEgracon\Form\Note;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEgracon\Service\Note\NoteService;
use UnicaenEgracon\Service\Pays\PaysService;

class NoteFormFactory {

    /**
     * @param ContainerInterface $container
     * @return NoteForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : NoteForm
    {
        /**
         * @var NoteService $noteService
         * @var PaysService $paysService
         * @var NoteHydrator $hydrator
         */
        $noteService = $container->get(NoteService::class);
        $paysService = $container->get(PaysService::class);
        $hydrator = $container->get('HydratorManager')->get(NoteHydrator::class);

        $form = new NoteForm();
        $form->setNoteService($noteService);
        $form->setPaysService($paysService);
        $form->setHydrator($hydrator);
        return $form;
    }
}