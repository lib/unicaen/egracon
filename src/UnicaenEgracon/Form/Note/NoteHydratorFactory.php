<?php

namespace UnicaenEgracon\Form\Note;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenEgracon\Service\Note\NoteService;
use UnicaenEgracon\Service\Pays\PaysService;

class NoteHydratorFactory {

    /**
     * @param ContainerInterface $container
     * @return NoteHydrator
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : NoteHydrator
    {
        /**
         * @var PaysService $paysService
         * @var NoteService $noteService
         */
        $noteService = $container->get(NoteService::class);
        $paysService = $container->get(PaysService::class);

        $hydrator = new NoteHydrator();
        $hydrator->setNoteService($noteService);
        $hydrator->setPaysService($paysService);
        return $hydrator;
    }
}