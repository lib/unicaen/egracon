<?php

namespace UnicaenEgracon\Form\Note;

use Laminas\Form\Element\Button;
use Laminas\Form\Element\Select;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use UnicaenEgracon\Service\Note\NoteServiceAwareTrait;
use UnicaenEgracon\Service\Pays\PaysServiceAwareTrait;

class NoteForm extends Form {
    use NoteServiceAwareTrait;
    use PaysServiceAwareTrait;

    public function init()
    {
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'valeur_basse',
            'options' => [
                'label' => "Valeur basse <span class='icon icon-obligatoire' title='Champ obligatoire'></span>:",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'valeur_basse',
            ],
        ]);
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'valeur_haute',
            'options' => [
                'label' => "Valeur haute <span class='icon icon-obligatoire' title='Champ obligatoire'></span>:",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'valeur_haute',
            ],
        ]);
        //libelle
        $this->add([
            'type' => Text::class,
            'name' => 'description',
            'options' => [
                'label' => "Description :",
                'label_options' => [ 'disable_html_escape' => true, ],
            ],
            'attributes' => [
                'id' => 'description',
            ],
        ]);
        //pays
        $this->add([
            'type' => Select::class,
            'name' => 'pays',
            'options' => [
                'label' => "Pays <span class='icon icon-obligatoire' title='Champ obligatoire'></span>:",
                'label_options' => [ 'disable_html_escape' => true, ],
                'empty_option' => "Choisir un pays",
                'value_options' => $this->getPaysService()->getPaysAsOption(),
            ],
            'attributes' => [
                'id'                => 'pays',
                'class'             => 'bootstrap-selectpicker show-tick',
                'data-live-search'  => 'true',
            ],
        ]);
        //references
        $this->add([
            'type' => Select::class,
            'name' => 'reference',
            'options' => [
                'label' => "Note de référence <span class='icon icon-obligatoire' title='Champ obligatoire'></span>:",
                'label_options' => [ 'disable_html_escape' => true, ],
                'empty_option' => "Choisir une note",
                'value_options' => $this->getNoteService()->getNotesReferencesAsOption(),
            ],
            'attributes' => [
                'id'                => 'pays',
                'class'             => 'bootstrap-selectpicker show-tick',
                'data-live-search'  => 'true',
            ],
        ]);

        //button
        $this->add([
            'type' => Button::class,
            'name' => 'creer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        //inputfilter
        $this->setInputFilter((new Factory())->createInputFilter([
            'valeur_basse'               => [ 'required' => true,  ],
            'valeur_haute'               => [ 'required' => true,  ],
            'description'                => [ 'required' => false,  ],
            'pays'                       => [ 'required' => true,  ],
            'reference'                 => [ 'required' => false,  ],
        ]));
    }
}