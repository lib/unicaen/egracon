<?php

namespace UnicaenEgracon\Form\Pays;

use Laminas\Hydrator\HydratorInterface;
use UnicaenEgracon\Entity\Db\Pays;

class PaysHydrator implements HydratorInterface {

    /**
     * @param Pays $object
     * @return array
     */
    public function extract(object $object): array
    {
        $data = [
            'code' => $object->getCode(),
            'libelle' => $object->getLibelle(),
        ];
        return $data;
    }

    /**
     * @param array $data
     * @param Pays $object
     * @return Pays
     */
    public function hydrate(array $data, object $object) : object
    {
        $code = (isset($data['code']) AND trim($data['code']) !== '')?trim($data['code']):null;
        $libelle = (isset($data['libelle']) AND trim($data['libelle']) !== '')?trim($data['libelle']):null;

        $object->setCode($code);
        $object->setLibelle($libelle);
        return $object;
    }


}