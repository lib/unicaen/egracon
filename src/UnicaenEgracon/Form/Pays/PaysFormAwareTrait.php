<?php

namespace UnicaenEgracon\Form\Pays;

trait PaysFormAwareTrait {

    private PaysForm $paysForm;

    public function getPaysForm(): PaysForm
    {
        return $this->paysForm;
    }

    public function setPaysForm(PaysForm $paysForm): void
    {
        $this->paysForm = $paysForm;
    }


}