<?php

namespace UnicaenEgracon\Form\Pays;

use Psr\Container\ContainerExceptionInterface;
use Psr\Container\ContainerInterface;
use Psr\Container\NotFoundExceptionInterface;

class PaysFormFactory {

    /**
     * @param ContainerInterface $container
     * @return PaysForm
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : PaysForm
    {
        /** @var PaysHydrator $hydrator */
        $hydrator = $container->get('HydratorManager')->get(PaysHydrator::class);
        $form = new PaysForm();
        $form->setHydrator($hydrator);
        return $form;
    }
}