<?php

namespace UnicaenEgracon\Form\Pays;

use Psr\Container\ContainerInterface;

class PaysHydratorFactory {

    public function __invoke(ContainerInterface $container) : PaysHydrator
    {
        $hydrator = new PaysHydrator();
        return $hydrator;
    }
}